# k8s-components
// login with gcloud to run kubectl commands
# $CLUSTER_NAME is getting from build of cluster terraform gke {terraform.tfvars = [name,project]]
- gcloud container clusters get-credentials $CLUSTER_NAME --region us-west1 --project $PROJECT_ID
#add namespace or run $ kubectl project namespace
# $ apt-get install kubectl 
# run kubectl , but first connect to cluster master to run commands : run this
# gcloud container clusters list && gcloud config list project && gcloud projects list
# export CLUSTER_NAME="tfyavineapp" && export PROJECT_ID="projectjfpb2"
# kubectl config view # check configurations

- kubectl apply -f k8s/

- kubectl apply -f k8s/pods.yaml
- kubectl apply -f k8s/deployment.yaml
- kubectl apply -f k8s/service.yaml
- kubectl get svc
- kubectl get all
- kubectl apply -f k8s/ingress.yaml
- kubectl get ingress

* ======= POTENCY OF TWO NUMBERS =======

![web potencia dos numeros](img/web-potency.png?raw=true "Title")

![curl potencia dos numeros](img/curl-potency.png?raw=true "Title")


* ============ GREETING ================

![web greeting](img/web-greeting.png?raw=true "Title")

![curl  greeting](img/curl-greeting.png?raw=true "Title")

# Cloud SDK is available in package format for installation on Debian and Ubuntu systems. This package contains the gcloud, gcloud alpha, gcloud beta, gsutil, and bq commands only. It does not include kubectl or the App Engine extensions required to deploy an application using gcloud commands.
# https://cloud.google.com/sdk/docs/downloads-apt-get

# Credits 
https://github.com/alexandarp/terraform-gke [alexandarp]
